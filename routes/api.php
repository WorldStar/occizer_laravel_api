<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->get('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
        $api->post('updateuser', 'App\\Api\\V1\\Controllers\\UpdateUserController@updateuser');
        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');

    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to this item is only for authenticated user. Provide a token in your request!'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);

        $api->get('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
		$api->get('getuser', 'App\\Api\\V1\\Controllers\\UserController@getUser');
        $api->get('category', 'App\\Api\\V1\\Controllers\\CategoryListController@getlist');
        $api->get('occasion', 'App\\Api\\V1\\Controllers\\OccasionListController@getlist');
        $api->get('categoryproduct', 'App\\Api\\V1\\Controllers\\CategoryProductsController@getcategoryproducts');
        $api->get('occasionproduct', 'App\\Api\\V1\\Controllers\\OccasionProductsController@getoccasionproducts');
        $api->get('search', 'App\\Api\\V1\\Controllers\\SearchController@getsearchproducts');
        $api->get('product', 'App\\Api\\V1\\Controllers\\ProductDetailController@getproduct');
        $api->get('orderlist', 'App\\Api\\V1\\Controllers\\OrderListController@getorderlist');
        $api->get('orderdetail', 'App\\Api\\V1\\Controllers\\OrderDetailController@orderdetail');
        $api->post('cancelorder', 'App\\Api\\V1\\Controllers\\OrderCancelController@cancelorder');
        $api->post('cancelproduct', 'App\\Api\\V1\\Controllers\\CancelProductController@cancelproduct');
        $api->post('deleteorder', 'App\\Api\\V1\\Controllers\\OrderDeleteController@deleteorder');
        $api->post('updateorder', 'App\\Api\\V1\\Controllers\\OrderUpdateController@updateorder');
        $api->post('orderproduct', 'App\\Api\\V1\\Controllers\\OrderProductController@getorder');
        $api->get('paidticket', 'App\\Api\\V1\\Controllers\\PaidTicketListController@getticketlist');
        $api->get('shipticket', 'App\\Api\\V1\\Controllers\\ShipTicketListController@getticketlist');
        $api->post('payorder', 'App\\Api\\V1\\Controllers\\PayOrderController@payorder');
        $api->get('ticketdetail', 'App\\Api\\V1\\Controllers\\TicketDetailController@getticketdetail');
        $api->post('cancelticket', 'App\\Api\\V1\\Controllers\\CancelTicketController@cancelticket');
        $api->post('deleteticket', 'App\\Api\\V1\\Controllers\\DeleteTicketController@deleteticket');
        $api->get('totalprice/get', 'App\\Api\\V1\\Controllers\\TotalPriceController@gettotalprice');
        $api->get('recentproducts', 'App\\Api\\V1\\Controllers\\RecentProductController@getlist');
        $api->get('productlist', 'App\\Api\\V1\\Controllers\\ProductListController@getlist');
        //$api->get('productdetail/{id}', 'App\\Api\\V1\\Controllers\\ProductDetailController@getdetails');

    });
});
