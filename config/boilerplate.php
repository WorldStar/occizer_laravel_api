<?php

return [

    'sign_up' => [
        'release_token' => env('SIGN_UP_RELEASE_TOKEN'),
        'validation_rules' => [
            'first_name',
            'last_name',
            'email',
            'password',
            'fb_id',
            'bio',
            'gender',
            'dob',
            'pic',
            'country',
            'state',
            'city',
            'address',
            'postal',
            'company',
            'age',
            'email2',
            'contactno',
            'whatsapp',
            'wechat',
            'permissions',
            'status',
        ]
    ],

    'login' => [
        'validation_rules' => [
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'forgot_password' => [
        'validation_rules' => [
            'email' => 'required|email'
        ]
    ],

    'reset_password' => [
        'release_token' => env('PASSWORD_RESET_RELEASE_TOKEN', false),
        'validation_rules' => [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]
    ]

];
