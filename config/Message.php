<?php

return array(
    'success_no' => '200',

    //signup
    'signup_failed_no' => '210',
    'signup_success'=>'Successfully signup!',
    'no_firstname'=>'Please input first name and try again.',
    'no_lastname'=>'Please input last name and try again.',
    'no_email'=>'Please input email and try again.',
    'no_password'=>'Please input password and try again.',
    'no_country' =>'Please input country name and try again.',

    //login
    'login_failed_no' => '220',
    'inactive_user'=>'Now, You cannot access.',
    'login_failed'=>'You must sign up.',
    'internet_failed_no'=>'500',
    'internet_failed'=>'Can not connecdtion.',

    'auth_failed'=>'Auth is failed. Please try again',
    'invalid_login_email'=>'Invalid email. Please input correct email.',

    'notfind_user_no' => '225',
    'notfind_user_no' => 'Inpute correct token, or you must create account.',



    //category
    'empty_data_no' => '300',
    'input_error_no' => '400',
    'no_categryproducts'=>'It does not exist the product for this category',
    'no_category_id'=>'Please input category id and try again.',

    //occasion
    'no_occasionproducts'=>'It does not exist the product for this occasion',
    'no_occasion_id'=>'Please input occasion_id and try again.',

    //search
    'no_keyword' => 'Please input keyword and try again.',
    'no_search_products' => 'Nothing to show',

    //product detail
    'disable_product_no' => '301',
    'no_product'=>'It does not exist the product for this id.',
    'no_product_id'=>'Please input product_id and try again.',
    'disable_product' => 'Now, this product is disabled. Please try again later.',

    // order, ticket
    'no_amount'=>'Please input amount and try again',
    'disable_product' => 'Now, this product is disabled. Please try again later.',
    'no_location'=>'Please input location and try again',
    'product_amount'=>'Current product doesnot exist as much as you want.',
    'no_orders'=>'There are not ordered products.',
    'cancel_totalorder'=>'Your orders were cancelled successfully.',
    'no_order'=>'It does not exist order for this order_id.',
    'no_order_id'=>'Please input order_id and try again.',
    'no_change_no'=>'250',
    'no_change'=>'There are no any change in your first order items.',
    'no_total_orders_no'=>'350',
    'no_total_orders'=>'You does not order any products.',
    'no_paid_no'=>'350',
    'no_paid'=>'You does not pay any orders.',
    'cancel_pay'=>' Your ticket was cancelled successfully.',
    'no_ship'=>'For your paid ticket was not allocated shipper yet.',
    'no_ticket'=>'It does not exist data.',
    'no_detail_id'=>'Please input the sub ticket id and try again.',
    'no_ticket_id'=>'Please input ticket_id and try again.',
    'delete_ticket'=>'Your ticket was deleted from DB.',
    'cancel_product'=>'Your order for this product was cancelled successfully.'

);
