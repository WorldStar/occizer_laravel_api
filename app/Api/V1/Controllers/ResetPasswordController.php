<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
class ResetPasswordController extends Controller
{
    public function resetPassword(Request $request, JWTAuth $JWTAuth)
    {
        if (empty($request->get('email'))){
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_email')
            ]);
        } else {
            $email = $request->get('email');
        }

        if (empty($request->get('password'))){
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_password')
            ]);
        } else {
            $password = $request->get('password');
        }

        $user = User::where('email', '=', $email)->first();
        //DB::table('users')->where('email', '=', $email)->update(array('password', $password));

        $user->password = $password;
        $user->save();

        return response()->json([
            'status' => 'ok',
            'token' => $JWTAuth->fromUser($user)
        ]);
    }
}
