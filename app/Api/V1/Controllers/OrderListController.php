<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class OrderListController extends Controller
{
    public function getorderlist()
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;
        $totalorder = DB::table('occ_gifts')->where('customer_id', $currentuser_id)->where('status', 0)->first();
        if(empty($totalorder)){
            return response()->json([
                'status' => Config::get('Message.empty_data_no'),
                'items' => Config::get('Message.no_orders')
            ]);
        } else {
            $order_id = $totalorder->order_id;
            $orderlist = DB::table('occ_gifts_items as i')
                ->join('occ_gifts as g', 'g.order_id', '=', 'i.order_id')
                ->join('occ_products as p','p.id','=','i.product_id')
                ->join('users as u', 'p.admin_id','=','u.id')
                ->select('g.totalprice', 'i.id', 'i.id', 'i.order_id', 'p.name', 'i.amount', 'p.price')
                ->where('i.order_id', $order_id)->where('g.customer_id',$currentuser_id)->where('i.status', 0)->get();

            return response()->json([
                'status' => Config::get('Message.success_no'),
                'items' => $orderlist
            ]);
        }
    }
}
