<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class TotalPriceController extends Controller
{
    public function gettotalprice()
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;

        $paidlist = DB::table('occ_gifts_items as i')
            ->join('occ_gifts as g','g.order_id','=','i.order_id')
            ->join('occ_products as p', 'p.id', '=', 'i.product_id')
            ->select('p.name', 'p.price', 'i.amount', 'g.totalprice')->where('g.customer_id', $currentuser_id)->where('g.status', 0)->get();

        if (count($paidlist) == 0) {
            return response()->json([
                'status' => Config::get('Message.no_paidckets_no'),
                'items' => Config::get('Message.no_paidtickets')
            ]);
        } else {
            return response()->json([
                'status' => Config::get('Message.success_no'),
                'items' => $paidlist
            ]);

        }
    }
}
