<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Request;
use DB;
class OccasionListController extends Controller
{
    public function getlist(Request $request, JWTAuth $JWTAuth)
    {

        $occasion = DB::table('occ_occasions')->orderby('id', 'asc')->get();
        //print_r($occasion);
        return response()->json([
            'status' => Config::get('Message.success_no'),
            'items' => $occasion
        ]);
    }
}
