<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
class UpdateUserController extends Controller
{
    public function updateuser(Request $request, JWTAuth $JWTAuth)
    {
        if (empty($request->get('email'))){
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_email')
            ]);
        }

        if (empty($request->get('password'))){
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_password')
            ]);
        }

        //  inpute update fields;
        $email =  $request->get('email');
        $password =  $request->get('password');

        $user = User::where('email', '=', $email)->first();
        $user->password = $password;

        if(!empty($request->get('first_name'))) {
            $user->first_name = $request->get('first_name');
        }
        if(!empty($request->get('lastirst_name'))) {
            $user->last_name = $request->get('last_name');
        }
        if(!empty($request->get('fb_id'))) {
        $user->fb_id = $request->get('fb_id');
        }
        if(!empty($request->get('bio'))) {
            $user->bio = $request->get('bio');
        }
        if(!empty($request->get('gender'))) {
            $user->gender = $request->get('gender');
        }
        if(!empty($request->get('bio'))) {
            $user->bio = $request->get('bio');
        }
        if(!empty($request->get('dob'))) {
            $user->dob = $request->get('dob');
        }
        if(!empty($request->get('pic'))) {
            $user->pic = $request->get('pic');
        }
        if(!empty($request->get('country'))) {
            $user->country = $request->get('country');
        }
        if(!empty($request->get('state'))) {
            $user->state = $request->get('state');
        }
        if(!empty($request->get('country'))) {
            $user->country = $request->get('country');
        }
        if(!empty($request->get('city'))) {
            $user->city = $request->get('city');
        }
        if(!empty($request->get('address'))) {
            $user->address = $request->get('address');
        }
        if(!empty($request->get('postal'))) {
            $user->postal = $request->get('postal');
        }
        if(!empty($request->get('company'))) {
            $user->company = $request->get('company');
        }
        if(!empty($request->get('postal'))) {
            $user->postal = $request->get('postal');
        }
        if(!empty($request->get('age'))) {
        $user->age = $request->get('age');
        }
        if(!empty($request->get('email2'))) {
            $user->email2 = $request->get('email2');
        }
        if(!empty($request->get('contactno'))) {
            $user->contactno = $request->get('contactno');
        }
        if(!empty($request->get('whatsapp'))) {
            $user->whatsapp = $request->get('whatsapp');
        }
        if(!empty($request->get('wechat'))) {
            $user->wechat = $request->get('wechat');
        }

//        $first_name = $request->get('first_name');
//        $last_name = $request->get('last_name');
//        $fb_id = $request->get('fb_id ');
//        $bio = $request->get('bio');
//        $gender = $request->get('gender');
//        $dob = $request->get('dob');
//        $pic = $request->get('picture');
//        $country = $request->get('country');
//        $state = $request->get('state');
//        $city = $request->get('city');
//        $address = $request->get('address');
//        $postal = $request->get('postal');
//        $company = $request->get('company');
//        $age = $request->get('age');
//        $email2 = $request->get('email2');
//        $contactno= $request->get('contactno');
//        $whatsapp = $request->get('whatsapp');
//        $wechat = $request->get('wechat');

        $user->save();

        return response()->json([
            'status' => Config::get('Message.success_no'),
            'token' => $JWTAuth->fromUser($user)
        ]);
    }
}
