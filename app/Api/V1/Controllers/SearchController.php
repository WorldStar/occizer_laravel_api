<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class SearchController extends Controller
{
    public function getsearchproducts(Request $request)
    {
        if(!empty($request->get('keyword'))){
            $keyword = $request->get('keyword');
            $searchproduct = DB::table('occ_products')->where('name','LIKE','%'. $keyword.'%')->where('state',1)->orderby('id', 'desc')->get();
            if(count($searchproduct) == 0){
                return response()->json([
                    'status' => Config::get('Message.empty_data_no'),
                    'items' =>  Config::get('Message.no_search_products')
                ]);
            }else{
                return response()->json([
                    'status' => Config::get('Message.success_no'),
                    'items' => $searchproduct
                ]);
            }
        }else{
            return response()->json([
                'status' => Config::get('Message.empty_data_no'),
                'items' =>  Config::get('Message.no_keyword')
            ]);
        }
    }
}
