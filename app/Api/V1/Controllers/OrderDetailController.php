<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class OrderDetailController extends Controller
{
    public function orderdetail(Request $request)
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;
        if(!empty($request->get('id'))){
            $order_id = $request->get('id');
            $order = DB::table('occ_gifts_items')->where('id',$order_id)->first();
            if(empty($order)){
                return response()->json([
                    'status' =>Config::get('Message.empty_data_no'),
                    'items' =>Config::get('Message.no_order')
                ]);
            } else {
                $order = DB::table('occ_gifts_items as i')
                    ->join('occ_gifts as g','g.order_id','=','i.order_id')
                    ->join('occ_products as p','p.id','=','i.product_id')
                    ->join('users as u', 'p.admin_id','=','u.id')
                    ->select('i.id', 'i.order_id', 'p.name', 'p.price', 'i.amount', 'u.first_name as vendor_firstname','u.last_name as vendor_lastname','u.country','u.company')
                    ->where('i.id','=', $order_id)->first();
                return response()->json([
                    'status' =>Config::get('Message.success_no'),
                    'items' =>$order
                ]);
            }
        } else {
            return response()->json([
                'status' =>Config::get('Message.input_error_no'),
                'items' =>Config::get('Message.no_order_id')
            ]);
        }
    }
}
