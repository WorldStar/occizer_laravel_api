<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class PayOrderController extends Controller
{
    public function payorder(Request $request)
    {
        $customer = JWTAuth::parseToken()->authenticate();
        $customer_id = $customer->id;

        $totalorder = DB::table('occ_gifts')->where('customer_id', $customer_id)->where('status', 0)->first();
        if (empty($totalorder)) {
            return response()->json([
                'status' => Config::get('Message.no_total_orders_no'),
                'items' => Config::get('Message.no_total_orders')
            ]);
        } else {

            DB::table('occ_gifts')->where('order_id', $totalorder->order_id)->where('status', 0)->update(array('status'=>1));

            //register in table-occ_tickets;
            $ticket_id = time() + rand(10, 99);
            $cutomer_id = $customer_id;
            $order_id = $totalorder->order_id;
            $ship_id = rand(1,3);
            $transaction_id = '';
            $transaction_fee = '';
            $description = '';
            $totalprice = $totalorder->totalprice;
            $date =  date('Y-m-d H:i:s');

            DB::table('occ_tickets')->insert(array('ticket_id'=>$ticket_id, 'customer_id'=>$cutomer_id, 'order_id'=>$order_id, 'ship_id'=>$ship_id, 'transaction_id'=>$transaction_id, 'transaction_fee'=>$transaction_fee, 'description'=>$description, 'totalprice'=>$totalprice, 'state'=>1));

            // for each order, update status to 1 in table-occ_gifts_items;
            $orders = DB::table('occ_gifts_items')->where('order_id', $totalorder->order_id)->where('status', 0)->get();
            foreach ($orders as $order) {
                if(!empty($order)){
                    DB::table('occ_gifts_items')->where('id', $order->id)->where('status', 0)->update(array('status'=>1));
                }
            }

            $paidticket = DB::table('occ_tickets')->where('customer_id',$customer_id)->where('state',1)->first();
            return response()->json([
                'status' => Config::get('Message.success_no'),
                'items' => $paidticket
            ]);
        }
    }
}
