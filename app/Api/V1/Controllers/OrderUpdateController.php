<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class OrderUpdateController extends Controller
{
    public function updateorder(Request $request)
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;

        if(!empty($request->get('id'))){
            $id = $request->get('id');
            $order = DB::table('occ_gifts_items')->where('id', $id)->where('status', 0)->first();

            if(empty($order)){
                return response()->json([
                    'status' =>Config::get('Message.empty_data_no'),
                    'items' =>Config::get('Message.no_order')
                ]);
            } else {
                $order_id = $order->order_id;
                $totalorder = DB::table('occ_gifts')->where('order_id', $order_id)->first();
                if (empty($request->get('amount'))) {
                    $amount = $order->amount;
                }else {
                    $amount = $request->get('amount');
                    $product = DB::table('occ_gifts_items as i')
                        ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                        ->select('p.id', 'p.amount', 'p.price')->where('i.id', $id)->first();
                    $new_amount = ($product->amount) - ($amount-($order->amount));

                    if ($new_amount <= 0) {
                        return response()->json([
                            'status' => Config::get('Message.input_error_no'),
                            'items' => Config::get('Message.product_amount')
                        ]);
                    } else{
                        $date = date('Y-m-d H:i:s');
                        $totalprice = ($totalorder->totalprice) + ($amount - ($order->amount)) * ($product->price);
                        DB::table('occ_products')->where('id', $product->id)->update(array('amount' => $new_amount));
                        DB::table('occ_gifts_items')->where('id', $id)->update(array('amount' => $amount,'updated_at'=>$date));
                        DB::table('occ_gifts')->where('order_id', $order_id)->update(array('totalprice' => $totalprice, 'updated_at'=>$date));
                    }
                }

                if(($amount == ($order->amount))) {
                    return response()->json([
                        'status' =>Config::get('Message.no_change_no'),
                        'items' =>Config::get('Message.no_change')
                    ]);;
                }else{
                    $order = DB::table('occ_gifts_items')->where('id',$id)->first();
                    return response()->json([
                        'status' =>Config::get('Message.success_no'),
                        'items' =>$order
                    ]);
                }
            }
        } else {
            return response()->json([
                'status' =>Config::get('Message.input_error_no'),
                'items' =>Config::get('Message.no_order_id')
            ]);
        }
    }
}
