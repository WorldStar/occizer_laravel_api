<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;

class OccasionProductsController extends Controller
{
    public function getoccasionproducts(Request $request)
    {
        if(!empty($request->get('id'))){
            $occasion_id = $request->get('id', 0);
            $occasionproducts = DB::table('occ_products')->where('occ_id', 'LIKE', '%'.$occasion_id.'%' )->where('state',1)->orderby('id', 'asc')->get();

            $products = array();
            foreach($occasionproducts as $occasionproduct){
                if(!empty($occasionproduct)){
                    $occasion1 = $occasionproduct->occ_id;
                    $occasion = explode(',', $occasion1);
                    if(in_array("$occasion_id", $occasion)){
                        array_push($products, $occasionproduct);
                    }
                }
            }
            if(count($products) == 0){
                return response()->json([
                    'status' => Config::get('Message.empty_data_no'),
                    'items' =>  Config::get('Message.no_occasionproducts')
                ]);
            }else{
                return response()->json([
                    'status' => Config::get('Message.success_no'),
                    'items' => $products
                ]);
            }
        }else{
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' =>  Config::get('Message.no_occasion_id')
            ]);
        }
    }
}
