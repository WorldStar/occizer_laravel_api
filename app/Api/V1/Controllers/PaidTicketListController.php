<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class PaidTicketListController extends Controller
{
    public function getticketlist()
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;

        $paid_ticket = DB::table('occ_tickets as t')
            ->join('occ_gifts_items as i','i.order_id','=','t.order_id')
            ->join('occ_products as p', 'p.id','=','i.product_id')
            ->select('i.id', 't.ticket_id', 't.totalprice', 'p.name', 'p.price', 'i.amount', 't.state')
            ->where('t.customer_id',$currentuser_id)->where('t.state', 1)->get();

        if (count($paid_ticket) == 0) {
            return response()->json([
                'status' => Config::get('Message.no_paid_no'),
                'items' => Config::get('Message.no_paid')
            ]);
        } else {
            return response()->json([
                'status' => Config::get('Message.success_no'),
                'items' => $paid_ticket
            ]);

        }
    }
}
