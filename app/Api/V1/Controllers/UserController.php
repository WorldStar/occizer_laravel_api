<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Request;


class UserController extends Controller
{
    public function getUser(Request $request, JWTAuth $JWTAuth)
    {
		//$token = $request->get('token');

        $user = $JWTAuth->toUser();

        return response()->json([
            'status' => Config::get('Message.success_no'),
            'userinfo' => $user
        ]);
    }
}
