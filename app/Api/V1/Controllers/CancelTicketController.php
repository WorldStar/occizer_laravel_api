<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class CancelTicketController extends Controller
{
    public function cancelticket(Request $request)
    {
        if(!empty($request->get('id'))) {
            $id = $request->get('id');
            $currentuser = JWTAuth::parseToken()->authenticate();
            $currentuser_id = $currentuser->id;

            $ticket = DB::table('occ_tickets')->where('id', $id)->where('customer_id', $currentuser_id)->where('state', '<>',0)->first();
            if(empty($ticket)){
                return response()->json([
                    'status' => Config::get('Message.empty_data_no'),
                    'items' => Config::get('Message.no_ticket')
                ]);
            } else{
                $order_id = $ticket->order_id;
                $products = DB::table('occ_tickets as t')
                    ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                    ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                    ->select('i.product_id', 'i.amount')->where('t.id', $id)->where('t.state', '<>',0)->get();

                if (!empty($products)) {
                    // in table-occ_products, increase amount;
                    foreach ($products as $product) {
                        $product_id = $product->product_id;
                        $amount = $product->amount;
                        DB::table('occ_products')->where('id', $product_id)->increment('amount', $amount);
                    }
                }

                DB::table('occ_tickets')->where('id', $id)->where('state', '<>',0)->update(array('state'=>0));
                DB::table('occ_gifts')->where('order_id', $order_id)->where('status','<>',0)->update(array('status'=>-1));
                DB::table('occ_gifts_items')->where('order_id', $order_id)->where('status','<>',0)->update(array('status'=>-1));

                return response()->json([
                    'status' => Config::get('Message.success_no'),
                    'items' => Config::get('Message.cancel_pay')
                ]);
            }

        } else {
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_ticket_id')
            ]);
        }
    }
}
