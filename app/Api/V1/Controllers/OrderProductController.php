<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use mysqli;
use DB;



class OrderProductController extends Controller
{
    public function getorder(Request $request)
    {
        //require input;
        if (empty($request->id)) {
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_product_id')
            ]);
        }

        if (empty($request->amount)) {
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_amount')
            ]);
        }

        if (empty($request->location)) {
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_location')
            ]);
        }

        // find total order_id;
        $customer = JWTAuth::parseToken()->authenticate();
        $customer_id = $customer->id;
        $orders = DB::table('occ_gifts_items as i')
            ->join('occ_gifts as g', 'g.order_id', '=', 'i.order_id')
            ->select('i.order_id')->where('g.customer_id', $customer_id)->where('g.status', 0)->first();
        if(empty($orders)){
            $order_id =  time()+($customer->id);
        } else{
            $order_id = $orders->order_id;
        }

        //description;
        if(!empty($request->descrition)){
           $description = $request->get('descrition');
        } else {
           $description = '';
        }

        $product_id = $request->id;
        $product = DB::table('occ_products')->where('id', $product_id)->first();

        // product state;
        if (empty($product)) {
            return response()->json([
                'status' => Config::get('Message.empty_data_no'),
                'items' => Config::get('Message.no_product')
            ]);
        }
        if (($product->state) == 0){
            return response()->json([
                'status' => Config::get('Message.empty_data_no'),
                'items' => Config::get('Message.disable_product')
            ]);
        }

        $category_id = $product->category_id;
        $amount = $request->amount;

        //when ordered amount is greater than existed amount;
        if((($product->amount)-$amount) < 0) {
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.product_amount')
            ]);
        }

        //delivery location;s
        $location = $request->location;
        $location = '35.0402409,133.3060391';

        $date = date('Y-m-d H:i:s');

        //$updated_at = date('Y-m-d H:i:s');;

        // insert total orders to table-occ_gifts_items;
        DB::table('occ_gifts_items')->insert(
            array('order_id'=> $order_id, 'category_id'=>$category_id, 'product_id'=>$product_id, 'amount'=>$amount, 'status'=>0)
        );

        //  insert total orders to table-occ_gifts;
        $orders = DB::table('occ_gifts_items')->where('order_id', $order_id)->get();
        if(empty($orders)) {
            $totalprice = ($product->price)*($orders->amount);
            DB::table('occ_gifts')->insert(
                array('order_id'=>$order_id, 'customer_id'=>$customer_id, 'description'=>$description, 'totalprice'=>$totalprice, 'delivery_location'=>$location, 'status'=>0)
            );
        } else {
            // for multi order, delete origin;
            DB::table('occ_gifts')->where('order_id', $order_id)->delete();
            // migration totalprice;
            $totalprice = 0;
            foreach ($orders as $order) {
                if(!empty($order)){
                    $product = DB::table('occ_products')->where('id', ($order->product_id))->first();
                    $totalprice = $totalprice +($product->price)*($order->amount);
                }
            }
            // insert migrated order to table-occ_gifts;
            DB::table('occ_gifts')->insert(
                array('order_id'=>$order_id, 'customer_id'=>$customer_id, 'description'=>$description, 'totalprice'=>$totalprice, 'delivery_location'=>$location, 'status'=>0,'created_at'=>$date)
            );
        }

        //in table-occ_products, decrease amount;
        DB::table('occ_products')->where('id', $product_id)->decrement('amount',$amount);

        $order = array('customer_id'=> $customer_id, 'product_id'=>$product_id, 'amount'=>$amount, 'totalprice' =>$totalprice, 'delivery_location'=>$location);
        return response()->json([
            'status' => Config::get('Message.success_no'),
            'items' => $order
        ]);
    }
}
