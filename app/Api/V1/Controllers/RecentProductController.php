<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Request;
use DB;
class RecentProductController extends Controller
{
    public function getlist(Request $request, JWTAuth $JWTAuth)
    {
        $recentproduct = DB::table('occ_products')->orderby('created_at', 'desc')->take(3)->get();
        return response()->json([
            'status' => Config::get('Message.success_no'),
            'items' => $recentproduct
        ]);

       // DB::table('occ_products')->find(1)->games()->take(3)->skip(2)->get();
    }
}
