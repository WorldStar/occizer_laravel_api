<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class OrderCancelController extends Controller
{
    public function cancelorder(Request $request)
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;

        $totalorder = DB::table('occ_gifts')->where('customer_id', $currentuser_id)->where('status',0)->first();
        if (empty($totalorder)) {
            return response()->json([
                'status' => Config::get('Message.empty_data_no'),
                'items' => Config::get('Message.no_orders')
            ]);
        }

        $order_id = $totalorder->order_id;
        $orderlist = DB::table('occ_gifts_items')->where('order_id', $order_id)->where('status', 0)->get();

        if (count($orderlist) == 0) {
            return response()->json([
                'status' => Config::get('Message.empty_data_no'),
                'items' => Config::get('Message.no_orders')
            ]);
        } else {
            // in table-occ_products, increase amount;
            foreach($orderlist as $order){
                $amount = $order->amount;
                $product_id = $order->product_id;
                DB::table('occ_products')->where('id', $product_id)->increment('amount', $amount);
            }

            //delete table-occ_gifts and table-occ_gifts_items;
            DB::table('occ_gifts')->where('order_id',$order_id)->where('status', 0)->delete();
            DB::table('occ_gifts_items')->where('order_id',$order_id)->where('status', 0)->delete();

            return response()->json([
                'status' =>Config::get('Message.success_no'),
                'items' =>Config::get('Message.cancel_totalorder'),
            ]);
        }
    }
}
