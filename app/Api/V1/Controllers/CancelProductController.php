<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class CancelProductController extends Controller
{
    public function cancelproduct(Request $request)
    {
        if(!empty($request->get('id'))){
            $id = $request->get('id');
            $order = DB::table('occ_gifts_items')->where('id',$id)->first();

            if(empty($order)){
                return response()->json([
                    'status' =>Config::get('Message.empty_data_no'),
                    'items' =>Config::get('Message.no_order')
                ]);
            } else {
                $totalorder = DB::table('occ_gifts')->where('order_id', $order->order_id)->first();
                $totalorders = DB::table('occ_gifts_items')->where('order_id', $order->order_id)->get();
                $product = DB::table('occ_products')->where('id', $order->product_id)->first();

                DB::table('occ_products')->where('id', $order->product_id)->increment('amount', $order->amount);
                DB::table('occ_gifts_items')->where('id', $id)->delete();

                $delprice = ($product->price) * ($order->amount);
                if (count($totalorders) == 1) {
                    DB::table('occ_gifts')->where('order_id', $order->order_id)->delete();
                    return response()->json([
                        'status' => Config::get('Message.success_no'),
                        'items' => Config::get('Message.cancel_totalorder')
                    ]);
                } else {
                    DB::table('occ_gifts')->where('order_id', $order->order_id)->decrement('totalprice', $delprice);
                    return response()->json([
                        'status' => Config::get('Message.success_no'),
                        'items' => Config::get('Message.cancel_product')
                    ]);
                }
            }
        } else {
            return response()->json([
                'status' =>Config::get('Message.no_order_id_no'),
                'items' =>Config::get('Message.no_order_id')
            ]);
        }
    }
}
