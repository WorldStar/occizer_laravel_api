<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use DB;
use Illuminate\Http\Request;

class SignUpController extends Controller
{
    public function signUp(Request $request, JWTAuth $JWTAuth)
    {
        if (empty($request->get('first_name'))){
            return response()->json([
                'status' => Config::get('Message.signup_failed_no'),
                'items' => Config::get('Message.no_firstname')
            ]);
        }

        if (empty($request->get('last_name'))){
            return response()->json([
                'status' => Config::get('Message.signup_failed_no'),
                'items' => Config::get('Message.no_lastname')
            ]);
        }

        if (empty($request->get('email'))){
            return response()->json([
                'status' => Config::get('Message.signup_failed_no'),
                'items' => Config::get('Message.no_email')
            ]);
        }

        if (empty($request->get('password'))){
            return response()->json([
                'status' => Config::get('Message.signup_failed_no'),
                'items' => Config::get('Message.no_password')
            ]);
        }

        $user = new User($request->all());
        if(!$user->save()) {
            return response()->json([
                'status' => Config::get('Message.success_no'),
                'items' => Config::get('Message.signup_success')
            ]);
        }

        $date =  date('Y-m-d H:i:s');
        DB::table('role_users')->insertGetId(array('role_id'=>3, 'created_at'=>$date, 'updated_at'=>$date));

        if(!Config::get('boilerplate.sign_up.release_token')) {
            return response()->json([
                'status' => Config::get('Message.success_no'),
                'items' => Config::get('Message.signup_success')
            ], 201);
        }

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => Config::get('Message.success_no'),
            'token' => $token
        ], 201);
    }
}
