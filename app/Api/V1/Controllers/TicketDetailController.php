<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class TicketDetailController extends Controller
{
    public function getticketdetail(Request $request)
    {
        $currentuser = JWTAuth::parseToken()->authenticate();
        $currentuser_id = $currentuser->id;

        if(!empty($request->get('id'))){
            $id = $request->get('id');
            $ticket = DB::table('occ_gifts_items')->where('id',$id)->first();

            if (empty($ticket)){
                return response()->json([
                    'status' =>Config::get('Message.empty_data_no'),
                    'items' =>Config::get('Message.no_ticket')
                ]);
            }else {
                $product = DB::table('occ_products')->where('id', $ticket->product_id)->first();
                $name = $product->name;
                $amount = $ticket->amount;
                $totalprice = ($product->price)*$amount;
                $date = $ticket->created_at;
                if(($ticket->status) == 2) {
                    $status = 'shipped';
                } elseif (($ticket->status) == 1){
                    $status = 'paid';
                } else{
                    $status = 'cancelled';
                }

                $detail = array('name'=>$name, 'amount'=>$amount, 'totalprice'=>$totalprice, 'created_at'=>$date, 'status'=>$status );

                return response()->json([
                    'status' => Config::get('Message.success_no'),
                    'items' => $detail
                ]);
            }
        } else {
            return response()->json([
                'status' =>Config::get('Message.input_error_no'),
                'items' =>Config::get('Message.no_detail_id')
            ]);
        }
    }
}
