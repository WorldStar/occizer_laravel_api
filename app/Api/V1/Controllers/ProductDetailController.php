<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class ProductDetailController extends Controller
{
    public function getproduct(Request $request)
    {

        if(!empty($request->get('id'))){
            $product_id = $request->get('id',0);
            $product = DB::table('occ_products as p')
                ->join('users as u','p.admin_id','=','u.id')
                ->select('p.*', 'u.first_name as vendor_firstname', 'u.last_name as vendor_lastname', 'u.country', 'u.company')->where('p.id',$product_id)->first();
            if(empty($product)){
                return response()->json([
                    'status' => Config::get('Message.empty_data_no'),
                    'items' => Config::get('Message.no_product')
                ]);
            } else if(($product->state) == 0) {
                return response()->json([
                    'status' =>Config::get('Message.disable_product_no'),
                    'items' =>Config::get('Message.disable_product')
                ]);
            } else{
                return response()->json([
                    'status' =>Config::get('Message.success_no'),
                    'items' =>$product
                ]);
            }
        } else{
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' =>  Config::get('Message.no_product_id')
            ]);
        }
    }
}
