<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class CategoryProductsController extends Controller
{
    public function getcategoryproducts(Request $request)
    {
        if(!empty($request->get('id'))){
            $category_id = $request->get('id', 0);
            $categoryproducts = DB::table('occ_products')->where('category_id',$category_id)->where('state',1)->orderby('id', 'asc')->get();
            if(count($categoryproducts) == 0){
                return response()->json([
                    'status' => Config::get('Message.empty_data_no'),
                    'items' =>  Config::get('Message.no_categryproducts')
                ]);
            }else{
                return response()->json([
                    'status' => Config::get('Message.success_no'),
                    'items' => $categoryproducts
                ]);
            }
        }else{
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' =>  Config::get('Message.no_category_id')
            ]);
        }
    }
}
