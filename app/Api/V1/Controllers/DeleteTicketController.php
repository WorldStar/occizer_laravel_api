<?php

namespace App\Api\V1\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Config;
use App\User;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Api\V1\Requests\ResetPasswordRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use DB;
class DeleteTicketController extends Controller
{
    public function deleteticket(Request $request)
    {
        if(!empty($request->get('id'))) {
            $id = $request->get('id');
            $currentuser = JWTAuth::parseToken()->authenticate();
            $currentuser_id = $currentuser->id;
            $ticket = DB::table('occ_tickets')->where('id', $id)->where('customer_id', $currentuser_id)->where('state', 0)->first();
            if(empty($ticket)){
                return response()->json([
                    'status' => Config::get('Message.empty_data_no'),
                    'items' => Config::get('Message.no_ticket')
                ]);
            } else{
                $order_id = $ticket->order_id;

                DB::table('occ_tickets')->where('id', $id)->where('state', 0)->delete();
                DB::table('occ_gifts')->where('order_id', $order_id)->where('status',-1)->delete();
                DB::table('occ_gifts_items')->where('order_id', $order_id)->where('status', -1)->delete();

                return response()->json([
                    'status' => Config::get('Message.success_no'),
                    'items' => Config::get('Message.delete_ticket')
                ]);
            }
        } else {
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_ticket_id')
            ]);
        }
    }
}
