<?php

namespace App\Api\V1\Controllers;

use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Api\V1\Controllers\Sentinel;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    public function login(Request $request, JWTAuth $JWTAuth)
    {
        if (empty($request->get('email'))){
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_email')
            ]);
        }

        if (empty($request->get('password'))){
            return response()->json([
                'status' => Config::get('Message.input_error_no'),
                'items' => Config::get('Message.no_password')
            ]);
        }

        $credentials = $request->only(['email', 'password']);
        try {
            $token = $JWTAuth->attempt($credentials);
            if(!$token) {  // no token
                return response()->json([
                    'status' => Config::get('Message.login_failed_no'),
                    'items' => Config::get('Message.login_failed')
                ]);
            }
        } catch (JWTException $e) {  // server 500 error  -- internet failed error.
            return response()->json([
                'status' => Config::get('Message.internet_failed_no'),
                'items' => Config::get('Message.internet_failed')
            ]);
        }

        return response()->json([
            'statua' => Config::get('Message.success_no'),
            'token' => $token
        ]);
    }
}
